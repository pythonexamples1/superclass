

class SuperClass: 
    def __init__(self, x, y, z=None): 
        self.x = x 
        self.y = y 
        self.z = z 

class SubClass(SuperClass): 
    def __init__(self, x, y, w, z=None): 
        super().__init__(x, y, z) 
        self.w = w
